package ru.doom.wad.logic;

public class IWadParseException extends Exception {

	public IWadParseException(String message) {
		super(message);
	}
}
